'''
The script below is specifically meant for downloading so-called rad_nl25_rac_mfbs_5min data,
or in other words: Precipitation - 5 minute precipitation accumulations from climatological 
gauge-adjusted radar dataset for The Netherlands (1 km) in KNMI HDF5 format. The file names
are read from a file, meaning that a selection can be made beforehand.
'''
import requests
import os
import warnings

def main():
    # My KNMI access token - I stored it in a text file
    fn = os.path.join(os.environ["HOME"], ".kim", "token.txt")
    request_header = get_request_header(fn)
    
    # Make sure to send the API key with every HTTP request
    session = requests.Session()
    session.headers.update({"Authorization": request_header})
    base_url = "https://api.dataplatform.knmi.nl/open-data/v1/datasets/rad_nl25_rac_mfbs_5min/versions/2.0/files"
    
    # Now try to download the files with specific names from file "filelist.txt"
    with open("filelist.txt", "r") as f:
        for line in f:
            filename = line.strip()
            endpoint = base_url + "/%s/url" % filename
            get_file_response = session.get(endpoint)
            
            # retrieve download URL for dataset file
            if get_file_response.status_code != 200:
                warnings.warn(f"Unable to get file: {filename}")
                warnings.warn(get_file_response.content)
            else:
                download_url = get_file_response.json().get("temporaryDownloadUrl")
                directory = os.path.normpath("./data")
                download_file_from_temporary_download_url(download_url, directory, filename)

# Adopted in an adapted form from the module download_full_dataset
def download_file_from_temporary_download_url(download_url, directory, filename):
    try:
        with requests.get(download_url, stream=True) as r:
            r.raise_for_status()
            with open(f"{directory}/{filename}", "wb") as f:
                for chunk in r.iter_content(chunk_size=8192):
                    f.write(chunk)
    except Exception:
        print("Unable to download file using download URL")
        return False, filename

    print(f"Downloaded dataset file '{filename}'")
    return True, filename

def get_request_header(tokenfile: str) -> str:
    result: str = ""
    if not os.path.exists(tokenfile):
        raise IOError("Unable to find file " + tokenfile)
    else:
        with open(tokenfile, 'r') as f:
            result= f.readline()
    return result  

if __name__ == "__main__":
    main()