'''
The script below is for retrieving the names of the available files with data of so-called 
rad_nl25_rac_mfbs_5min data, or in other words: Precipitation - 5 minute precipitation
accumulations from climatological gauge-adjusted radar dataset for The Netherlands (1 km)
in KNMI HDF5 format. The names are printed to the console. From the obtained list, a
selection can be made. The list with selected names have to then be pasted into a text file.

'''
import requests
import os
import json

def main():
    base_url = "https://api.dataplatform.knmi.nl/open-data/v1/datasets/rad_nl25_rac_mfbs_5min/versions/2.0/files"

    # My KNMI access token - I stored it in a text file
    fn = os.path.join(os.environ["HOME"], ".kim", "token.txt")
    request_header = get_request_header(fn)
    
    # Make sure to send the API key with every HTTP request
    session = requests.Session()
    session.headers.update({"Authorization": request_header})
    
    # Prepare further
    params = {"Authorization": request_header}
    response = session.get(base_url, params=params)
    if response.status_code != 200:
        print("Error: " + response.status_code)
        jsdata = {"nextPageToken": ""}
    else:
        jsdata = get_data_as_json(response.content)
        files = jsdata["files"]
        for fdata in files:
            print(fdata["filename"])
    
    # Prepare to get further file names
    while jsdata["nextPageToken"] != "":
        max_keys = 500
        nextPageToken = jsdata["nextPageToken"]
        params = {"Authorization": request_header, "maxKeys": f"{max_keys}", "nextPageToken": nextPageToken}
        response = session.get(base_url, params=params)
        if response.status_code == 200:
            jsdata = get_data_as_json(response.content)
            files = jsdata["files"]
            for fdata in files:
                print(fdata["filename"])
            if "nextPageToken" in jsdata:
                nextpageToken = jsdata["nextPageToken"]
            else:
                jsdata = {"nextPageToken": ""}
        else:
            jsdata = {"nextPageToken": ""}
        print("Completed successfully!")

def get_request_header(tokenfile: str) -> str:
    result: str = ""
    if not os.path.exists(tokenfile):
        raise IOError("Unable to find file " + tokenfile)
    else:
        with open(tokenfile, 'r') as f:
            result= f.readline()
    return result     
 
def get_data_as_json(content: str):
    result = json.loads(content)
    return result

if __name__ == "__main__":
    main()

