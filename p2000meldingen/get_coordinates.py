import pandas as pd
import get_municipality as get_gem
#from .maps_api import MapsApi

import os
import json
import requests

# this script takes the dataset obtained from the P2000 server
# and enriches it with extra latlon info from PDOK
# based on the address. Furthermore, the website zoekplaats is
# used to obtain the municipality 

def load_data(file): 
    with open(file, 'r') as file:
        data = json.load(file)
        if 'data' in data:
            data = data['data']
        return data

def print_data():
    data = load_data('../data/parsed.json')
    for d in data:
        if len(d['address'].split(' ')) <= 1:
            print(d['message'], "-----", d['address'], "----- ")

def print_json(data):
    to_print = ''
    to_print += '['
    for d in data:
        to_print += json.dumps(d) + ",\n"


    to_print = to_print[0:-2]
    to_print += ']\n'
    print(to_print)

def main():
    """
    data = load_data('../data/parsed.json')
	key = os.environ['maps_key']
	api = MapsApi(key=key)
    
	print('[')
	for d in data:
		google_results = None
		try:
			google_results = api.get_coordinate(d['address'] + ", Netherlands")
		except Exception as e:
			try:
				google_results = api.get_coordinate(d['address'] + ", Netherlands")
			except Exception as e:
				try:
					google_results = api.get_coordinate(d['address'] + ", Netherlands")
				except Exception as e:
					pass

		if google_results and google_results['status'] == 'OK':
			d['google_results'] = google_results['results']
			print(json.dumps(d) + ",")
		else:
			print("ERROR")
			print(json.dumps(d) + ",")
	print(']')
    """
    basic_url = "https://api.pdok.nl/bzk/locatieserver/search/v3_1/free"
    url = basic_url + "?bq=type:address&q=%s"
    data = load_data('../data/parsed.json')
    print('[')
    for d in data:
        try:
            address = d['address']
            address = address.replace(' ', '+and+')
            response = requests.get(url % address)
            if response.status_code == 200:
                data = response.json()
                if ("response" in data) and ("docs" in data["response"]):
                    docs = data["response"]["docs"]
                    if len(docs) > 0:
                        geom = docs[0]["centroide_ll"]
                        lon, lat = geom[6:-1].split(' ')
                        gem = get_gem.do(d)
                        d["pdok_results"] = {"lat":lat, "lng":lon, "gemeente":gem}
            print(json.dumps(d) + ",")
        except Exception as e:
            print(e)
    print(']')
                
if __name__ == '__main__':
    # print_data()
    main()




