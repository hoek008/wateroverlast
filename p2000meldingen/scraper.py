import requests

from datetime import datetime, timedelta

import time
import json

def main():
	url = "http://api.alarmfase1.nl/3.2/calls/fire.json?&end={}"
	
	strfformat = "%Y-%m-%d %H:%M:%S"
	enddate = datetime(year=2022, month=5, day=3, hour=0, minute=0, second=0)  #month=12, day=31, hour=23, minute=59, second=59)
	prevdate = datetime.strptime('2022-01-01 00:00:00', strfformat) # datetime.now()
	tmpdate = enddate
	enddate = prevdate
	prevdate = tmpdate
	
	print('{"data":[')
	
	while  prevdate > enddate:
		# print(url.format(prevdate.strftime(strfformat)))
		data = requests.get(url.format(prevdate.strftime(strfformat)))
	
		if data.status_code != 200:
			data = requests.get(url.format(prevdate.strftime(strfformat)))
	
		if data.status_code != 200:
			data = requests.get(url.format(prevdate.strftime(strfformat)))
		
		try:	
			calls = data.json()['calls']
			for c in calls:
				print(json.dumps(c) + ",")
			prevdate = datetime.strptime(calls[-1]['date'], strfformat)
		except Exception as e:
			prevdate = datetime.strptime(get_last_date(data.text), strfformat)
	
		time.sleep(0.1)
	
	print("]}")

def get_last_date(datatext):
	# Assume a date string of 19 characters long
	idx = datatext.rindex("date")
	result = datatext[idx + 7: idx + 26]
	return result

if __name__ == "__main__":
    main()