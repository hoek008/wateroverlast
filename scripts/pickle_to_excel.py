#!/usr/bin/env python
# coding: utf-8
import pandas as pd
from pathlib import Path

path_to_pickled_data = Path().absolute().parent / "scripts" / "50-500.pkl"
if path_to_pickled_data.exists():
    df = pd.read_pickle(path_to_pickled_data)
    print(df)
else:
    print(f"File {path_to_pickled_data} does not exist!")

df.to_excel(Path().absolute().parent / "scripts" / "50-500.xlsx")





